/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.unj.dubbotest.provider;

import java.util.List;

/**
 * 类DemoService.java的实现描述：TODO 类实现描述
 * 
 * @author arron 2017年10月30日 下午1:06:48
 */
public interface DemoService {

    String sayHello(String name);

    public List getUsers();

}
