package com.empire.emsite.modules.swagger;

import java.util.ArrayList;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;

/**
 * swagger2测试Controller 
 * ClassName: PersonController 
 * @Description: TODO
 * @author Feelj
 * @date 2018年6月2日
 */
@Controller
@RequestMapping("/person")
@Api(tags="个人业务")
public class PersonController {

    @RequestMapping(value="/getPerson",method= RequestMethod.GET)
    public @ResponseBody List<String> getPersons() {
    	List<String> list=new ArrayList<>();
    	list.add("1");
    	list.add("2");
    	list.add("3");
    	list.add("4");
    	list.add("5");
        return list;
    }
}