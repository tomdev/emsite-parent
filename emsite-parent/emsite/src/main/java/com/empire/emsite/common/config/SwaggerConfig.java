package com.empire.emsite.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger2配置类，测试使用
 * ClassName: SwaggerConfig 
 * @Description: TODO
 * @author Feelj
 * @date 2018年6月1日
 */
@EnableWebMvc
@Configuration
@EnableSwagger2
@ComponentScan(basePackages="com.empire.emsite.modules.sys.web")
public class SwaggerConfig   {

	/*@Bean
    public Docket petApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(this.apiInfo())
                .select()
                //.apis(RequestHandlerSelectors.basePackage("com.empire.emsite.modules.sys.web"))
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();

    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("服务平台 API").description("").termsOfServiceUrl("http://localhost:8080").version("1.0").build();
    }*/
	
	@Bean
    public Docket buildDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(buildApiInf())
                .select()       
                //.apis(RequestHandlerSelectors.basePackage("com.empire.emsite.modules.sys.web"))//controller路径
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo buildApiInf(){
        return new ApiInfoBuilder()
                .title("swagger2测试")
                .termsOfServiceUrl("https://gitee.com/hackempire/emsite-parent")
                .description("springmvc swagger2")
                .contact(new Contact("hackempire", "https://gitee.com/hackempire/emsite-parent", "haotking@163.com"))
                .build();

    }
	
}