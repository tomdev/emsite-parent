/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.unj.dubbotest.provider.impl;

import java.io.Serializable;

/**
 * 类User.java的实现描述：TODO 类实现描述
 * 
 * @author arron 2017年10月30日 下午1:07:10
 */
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private int               age;
    private String            name;
    private String            sex;

    public User() {
        super();
    }

    public User(int age, String name, String sex) {
        super();
        this.age = age;
        this.name = name;
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

}
